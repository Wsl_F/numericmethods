package matix;

/**
 * Created by olessavluk on 10/8/14.
 */
public class Main {
    public static void main(String[] args) {
        Matrix m = new Matrix(5, Matrix.FILL_DIAGONAL);
        Solver solver = new Solver();

        System.out.println("==========Original===================");
        System.out.println(m.toString());

        System.out.println("==========Gauss===================");
        // solution exists
        Matrix gauss = solver.gauss(m);
        System.out.print(gauss.toString());
        System.out.println("Iterations: " + solver.iterations + "\n");

        solver = new Solver();
        // diagonal perseverance
        System.out.println("==========Jacobi===================");
        Matrix jacobi = solver.jacobi(m);
        System.out.print(jacobi.toString());
        System.out.println("Iterations: " + solver.iterations + "\n");

        solver = new Solver();
        // diagonal perseverance
        System.out.println("==========Seidel===================");
        Matrix seidel = solver.seidel(m);
        System.out.print(seidel.toString());
        System.out.println("Iterations: " + solver.iterations + "\n");

        solver = new Solver();
        // diagonal perseverance
        System.out.println("==========Relaxation===================");
        Matrix relaxation = solver.relaxation(m, 1.5);
        System.out.print(relaxation.toString());
        System.out.println("Iterations: " + solver.iterations + "\n");

        solver = new Solver();
        // for symmetrical matrix
        System.out.println("==========Square Root===================");
        Matrix square = solver.square(m);
        System.out.print(square.toString());
        System.out.println("Iterations: " + solver.iterations + "\n");
    }
}
