package matix;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Random;

/**
 * Created by olessavluk on 10/8/14.
 */
public class Matrix {
    public double[][] A; //matrix
    public double[] b;   //rigth vector
    public double[] x;   //solution

    public int size;
    public boolean solved = false;
    public boolean outB = true;

    static final int doubleLen = 10;
    static final int min = -5;
    static final int max = 5;

    public static final int FILL_RAND = 0;
    public static final int FILL_DIAGONAL = 1;
    public static final int FILL_HILBERT = 2;
    public static final int FILL_DIAGONAL_HILBERT = 3;
    public static final int FILL_SYMMETRIC = 4;
    public static final int FILL_SYMMETRIC_DIAGONAL_HILBERT = 5;

    public Matrix(int size) {
        this(size, Matrix.FILL_RAND, Matrix.min, Matrix.max);
    }

    public Matrix(Matrix m) {
        this(m.size);
        this.solved = m.solved;
        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                this.A[i][j] = m.A[i][j];
            }
            this.b[i] = m.b[i];
            this.x[i] = m.x[i];
        }
    }

    public Matrix(int size, int fillerType) {
        this(size, fillerType, Matrix.min, Matrix.max);
    }

    public Matrix(int size, int fillerType, double min, double max) {
        this.size = size;
        this.A = new double[size][size];
        this.b = new double[size];
        this.x = new double[size];
        if (fillerType == Matrix.FILL_RAND) {
            this.fillRandA(min, max);
            this.fillRandX(min, max);
            this.calculateB();
        } else if (fillerType == Matrix.FILL_DIAGONAL) {
            this.fillRandA(min, max);
            this.adjustDiagonal();
            this.fillRandX(min, max);
            this.calculateB();
        } else if (fillerType == Matrix.FILL_HILBERT) {
            this.fillHilbert();
            this.fillRandX(min, max);
            this.calculateB();
        } else if (fillerType == Matrix.FILL_DIAGONAL_HILBERT) {
            this.fillHilbert();
            this.adjustDiagonal();
            this.fillRandX(min, max);
            this.calculateB();
        } else if (fillerType == Matrix.FILL_SYMMETRIC) {
            this.fillRandA(min, max);
            this.adjustSymmetric();
            this.fillRandX(min, max);
            this.calculateB();
        } else if (fillerType == Matrix.FILL_SYMMETRIC_DIAGONAL_HILBERT) {
            this.fillHilbert();
            this.adjustSymmetric();
            this.adjustDiagonal();
            this.fillRandX(min, max);
            this.calculateB();
        } else {
            throw new NoSuchElementException("Undefined filler type");
        }
    }

    private void fillRandA(double minValue, double maxValue) {
        Random rand = new Random();
        double range = maxValue - minValue;
        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                this.A[i][j] = rand.nextDouble() * range + minValue;
            }
        }
    }

    private void fillHilbert() {
        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                this.A[i][j] = (double) 1 / (i + j + 2);
            }
        }
    }

    private void adjustDiagonal() {
        int sum;
        for (int i = 0; i < this.size; i++) {
            sum = 1;
            for (int j = 0; j < this.size; j++) {
                sum += Math.abs(i == j ? 0 : this.A[i][j]);
            }
            while (this.A[i][i] < sum) this.A[i][i] += sum;
        }
    }

    private void adjustSymmetric() {
        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < i; j++) {
                this.A[i][j] = this.A[j][i];
            }
        }
    }

    private void fillRandX(double minValue, double maxValue) {
        Random rand = new Random();
        double range = maxValue - minValue;

        for (int i = 0; i < this.size; i++) {
            this.x[i] = rand.nextDouble() * range + minValue;
        }
    }

    private void calculateB() {
        double sum;
        for (int i = 0; i < this.size; i++) {
            sum = 0;
            for (int j = 0; j < this.size; j++) {
                sum += this.A[i][j] * this.x[j];
            }
            this.b[i] = sum;
        }
        this.solved = true;
    }

    public void swapRows(int a, int b) {
        double[] tmp;
        tmp = this.A[a];
        this.A[a] = this.A[b];
        this.A[b] = tmp;

        double tmpB = this.b[a];
        this.b[a] = this.b[b];
        this.b[b] = tmpB;
    }

    public void doTranspose() {
        for (int i = 0; i < this.size; i++) {
            for (int j = i + 1; j < this.size; j++) {
                double tmp = this.A[i][j];
                this.A[i][j] = this.A[j][i];
                this.A[j][i] = tmp;
            }
        }
    }

    public Matrix multiply(Matrix m) {
        Matrix res = new Matrix(this.size, Matrix.FILL_RAND, 0, 0);
        res.copyB(this);
        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                for (int k = 0; k < this.size; k++) {
                    res.A[i][j] += this.A[i][k] * m.A[k][j];
                }
            }
        }
        return res;
    }

    public void copyB(Matrix m) {
        for (int i = 0; i < m.size; i++) {
            this.b[i] = m.b[i];
        }
    }

    public static String formatDouble(double num) {
        DecimalFormat df = new DecimalFormat("#0.00000");
        String res = df.format(num);
        while (res.length() < doubleLen) {
            res = " " + res;
        }
        return res;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder("matrix:\n");
        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                res.append(" ").append(Matrix.formatDouble(A[i][j]));
            }
            if (this.outB) {
                res.append(" | ").append(formatDouble(b[i]));
            }
            res.append("\n");
        }

        if (this.solved) {
            res.append(this.solutionToString());
        }
        return res.toString();
    }

    public String solutionToString() {
        StringBuilder res = new StringBuilder("solution:\n");
        for (int i = 0; i < this.size; i++) {
            res.append(" ").append(Matrix.formatDouble(this.x[i]));
        }
        res.append("\n");
        return res.toString();
    }
}
