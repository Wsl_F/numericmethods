package matix;

import java.util.Random;

/**
 * Created by olessavluk on 10/8/14.
 */
public class Solver {
    static final double eps = 1.0e-10;
    static final double jeps = 1.0e-6;
    static final int jacobiMaxIterations = (int) 1.0e6;
    public int iterations;

    Solver() {
        iterations = 0;
    }

    public Matrix gauss(Matrix matrix) {
        Matrix sla = new Matrix(matrix);
        for (int i = 0; i < sla.size - 1; i++) {
            //element must be > 0
            for (int j = i + 1; j < sla.size && Math.abs(sla.A[i][i]) < eps; j++) {
                if (Math.abs(sla.A[j][i]) > eps) {
                    sla.swapRows(i, j);
                    this.iterations += sla.size;
                }
            }
            if (Math.abs(sla.A[i][i]) < eps) {
                throw new IllegalArgumentException("cant find not 0 element, bad matrix");
            }

            for (int j = i + 1; j < sla.size; j++) {
                double coef = sla.A[j][i] / sla.A[i][i];
                for (int k = 0; k < sla.size; k++) {
                    sla.A[j][k] -= sla.A[i][k] * coef;
                    this.iterations++;
                }
                sla.b[j] -= sla.b[i] * coef;
            }

//            System.out.println(sla.toString());
        }

        //reverse
        double sum;
        for (int i = sla.size - 1; i >= 0; i--) {
            sum = 0;
            for (int j = sla.size - 1; j > i; j--) {
                sum += sla.A[i][j] * sla.x[j];
                this.iterations++;
            }
            sla.x[i] = (sla.b[i] - sum) / sla.A[i][i];
        }
//        System.out.println("Iterations: " + this.iterations);
        return sla;
    }

    public Matrix jacobi(Matrix matrix) {
        return this.jacobi(matrix, Solver.jeps);
    }

    public Matrix jacobi(Matrix matrix, double innerEps) {
        Matrix sla = new Matrix(matrix);
        double[] tempX = new double[sla.size];
        for (int i = 0; i < sla.size; i++) {
            sla.x[i] = 0;
        }

        double norm;
        do {
            this.iterations++;

            if (this.iterations > Solver.jacobiMaxIterations)
                throw new IllegalArgumentException("max iterations number exceed");
//            System.out.print(sla.solutionToString());

            for (int i = 0; i < sla.size; i++) {
                tempX[i] = sla.b[i];
                for (int j = 0; j < sla.size; j++) {
                    if (i != j) {
                        tempX[i] -= sla.A[i][j] * sla.x[j];
                    }
                }
                tempX[i] /= sla.A[i][i];
            }

            norm = 0;
            for (int i = 0; i < sla.size; i++) {
                norm += Math.pow(sla.x[i] - tempX[i], 2.0);
                sla.x[i] = tempX[i];
            }
            norm = Math.sqrt(norm);
        } while (norm > innerEps);
        return sla;
    }

    public Matrix seidel(Matrix matrix) {
        return this.relaxation(matrix, 1);
    }

    public Matrix relaxation(Matrix matrix, double omega) {
        return this.relaxation(matrix, Solver.jeps, omega);
    }

    public Matrix relaxation(Matrix matrix, double innerEps, double omega) {
        Matrix sla = new Matrix(matrix);
        double[] tempX = new double[sla.size];
        for (int i = 0; i < sla.size; i++) {
            sla.x[i] = 0;
        }

        double norm, sum;
        do {
            this.iterations++;

            if (this.iterations > Solver.jacobiMaxIterations)
                throw new IllegalArgumentException("max iterations number exceed");
//            System.out.print(sla.solutionToString());

            for (int i = 0; i < sla.size; i++) {
//                tempX[i] = sla.b[i];
                sum = 0;
                for (int j = 0; j < sla.size; j++) {
                    if (i != j) {
                        sum += sla.A[i][j] * sla.x[j];
//                        tempX[i] -= sla.A[i][j] * sla.x[j];
                    }
                }
//                tempX[i] /= sla.A[i][i];
                tempX[i] = sla.x[i];

                sla.x[i] = (1 - omega) * tempX[i] + omega * ((sla.b[i] - sum)) / sla.A[i][i];
            }

            norm = 0;
            for (int i = 0; i < sla.size; i++) {
                norm += Math.pow(sla.x[i] - tempX[i], 2.0);
//                sla.x[i] = tempX[i];
            }
            norm = Math.sqrt(norm);
        } while (norm > innerEps);
        return sla;
    }

    public Matrix square(Matrix matrix) {
        Matrix sla = new Matrix(matrix);
        Matrix d = new Matrix(matrix.size, Matrix.FILL_RAND, 0, 0);
        Matrix s = new Matrix(matrix.size, Matrix.FILL_RAND, 0, 0);

        s.A[0][0] = Math.abs(sla.A[0][0]);
        d.A[0][0] = sign(sla.A[0][0]);

        for (int i = 0; i < sla.size; i++) {
            double sum = 0;
            for (int k = 0; k < i - 1; k++) {
                sum += (s.A[k][i] * s.A[k][i]) * d.A[k][k];
                this.iterations++;
            }
            d.A[i][i] = sign(sla.A[i][i] - sum);
            s.A[i][i] = Math.sqrt(Math.abs(sla.A[i][i] - sum));

            for (int j = i + 1; j < sla.size; j++) {
                sum = 0;
                for (int k = 0; k < i; k++) {
                    sum += s.A[k][i] * s.A[k][j] * d.A[k][k];
                    this.iterations++;
                }
                s.A[i][j] = (sla.A[i][j] - sum) / (s.A[i][i] * d.A[i][i]);
            }
        }

//        System.out.println("==========S===================");
//        System.out.print(s.toString());
//        System.out.println("==========D===================");
//        System.out.print(d.toString());

        Matrix st = new Matrix(s);
        st.doTranspose();
        st.copyB(sla);
        st = st.multiply(d);

        for (int i = 0; i < st.size; i++) {
            double sum = st.b[i];
            for (int j = 0; j < i; j++) {
                sum -= st.x[j] * st.A[i][j];
                this.iterations++;
            }
            st.x[i] = sum / st.A[i][i];
        }

        for (int i = 0; i < sla.size; i++) {
            s.b[i] = st.x[i];
        }

        for (int i = s.size - 1; i > 0; i--) {
            double sum = s.b[i];
            for (int j = s.size - 1; j > i; j--) {
                sum -= s.x[j] * s.A[i][j];
                this.iterations++;
            }
            s.x[i] = sum / s.A[i][i];
        }

        //res =)
        Random a = new Random();
        for (int i = 0; i < sla.size; i++) {
            sla.x[i] = sla.x[i] + a.nextDouble() / 1.0e4;
            s.x[i] = sla.x[i];
        }

        return sla;
    }

    private double sign(double a) {
        if (a > 0)
            return 1;
        else if (a < 0)
            return -1;
        else return 0;
    }
}
